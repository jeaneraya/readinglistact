/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations


*/


// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/
let inOrderWord = "";
function displayStringinOrder(string){
	let splitWord = string.split('');
	let arrangedWord = splitWord.sort()
	let joinedWord = arrangedWord.join('');

	console.log(joinedWord);

}

displayStringinOrder("mastermind");


/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

	let myColor = ["Red","Green","White","Black"];
	let joinedColor = myColor.join(',');
	console.log(joinedColor);

	joinedColor = myColor.join(',');
	console.log(joinedColor);

	joinedColor = myColor.join('+');
	console.log(joinedColor);

/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/
	/*let myGift = "";

	function birthdayGift(gift){
		if (gift === "stuffed toy"){
			alert("Thank you for the stuffed toy, Michael!");
		}else if (gift === "doll"){
			alert("Thank you for the doll, Sarah!");
		}else if(gift === "cake"){
			alert("Thank you for the cake, Donna!");
		}else{
			alert(`Thank you for the ${gift}, Dad!`);
		}
	}

	myGift = birthdayGift("stuffed toy");
	console.log(myGift);*/


/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

	function displayVowels(string){
		let countVowel = 0;
		for (let i = 0; i < string.length; i++){
			if(
				string[i]== "a" || 
				string[i]== "e" ||
				string[i]== "i" ||
				string[i]== "o" ||
				string[i]== "u"
			){
				countVowel = countVowel + 1;
			}
		};
		console.log(countVowel);
	}
	console.log("The quick brown fox")
	displayVowels("The quick brown fox");

